<!DOCTYPE html>
<html>
    <head>
        <h3>Buat Account Baru</h3>
    </head>
    <body>
        <h4>Sign Up Form</h4>
        <form action="/home">
            <label>First Name :</label><br>
            <input type="text" name="name"><br><br>
            <label>Last Name :</label><br>
            <input type="text" name="last_name"><br><br>
            <label>Gender</label><br>
            <input type="radio" name="male">Male<br>
            <input type="radio" name="female">Female<br><br>
            <label>Nationality</label><br>
            <select name="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Singapura">Singapura</option>
                <option value="Thailand">Thailand</option><br><br>
            </select><br><br>
            <label>Language Spoken</label><br>
            <input type="checkbox" name="Bahasa Indonesia">Bahasa Indonesia<br>
            <input type="checkbox" name="English">English<br>
            <input type="checkbox" name="Other">Other<br><br>

            <label>Bio</label><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br>
            <button type="submit">Sign Up</button><br><br>
        </form>
    </body>
</html>